#!/bin/bash
# $Id: kick,v 0.0 $
# $Source: kick,v $
# Kick into waitin processes to start another build cycle.
#
# Copyright (c) 2022 Radek Hnilica
# All rights reserved.
# License: GPLv3, or ask me.

rm -v mark.ts
kill $(<sleep.pid)
