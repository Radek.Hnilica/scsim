#!/bin/bash
# $Id: cycle.sh,v 1.7 2023-10-17 18:34:05+02 radek Exp $
# $Source: /home/radek/st/repo/sc/src/scsim/src/cycle.sh,v $

typeset -r BUILD_STAMP=mark.ts
typeset -r TITLE=scsim
typeset -r BIG_LOOP="10 minutes ago"
typeset -r DELAY=30s

while :; do
	# Check the time from last make
	if [[ $(date +%F_%T -r $BUILD_STAMP) < $(date +%F_%T -d "$BIG_LOOP") ]]; then
		echo
		title "$TITLE building"
		make	   # Build the outputs, binaries, reports, ...
		touch $BUILD_STAMP
		# Now we need to process the assembly source files
		./build-fw.sh
		# And also slice some of the listings
		./cut-blocks.sh sysfw/sc65rom.lst sysfw/sc65rom
		echo -n "INFO: sleeping"
	fi
	echo -n .
	title "$TITLE sleeping"
	sleep $DELAY &
	sleep_pid=$!
	echo $sleep_pid >sleep.pid
	wait $sleep_pid
done
