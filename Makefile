# ! make
# $Id: Makefile,v 1.8 2023-10-17 18:23:16+02 radek Exp $
# $Source: /home/radek/st/repo/sc/src/scsim/src/Makefile,v $

# Get the system os we are running on
SYSTEM=$(shell uname -s)

# On MacBook I'm using different compiler and need to define it there.
ifeq ($(SYSTEM),Darwin)
CC      = gcc-12
endif

STD	:= -std=c11 -g
STACK	:=
WARNS	:= -Wall -Wextra -pedantic -Wconversion
CFLAGS	:= $(STD) $(STACK) $(WARNS)

# Ad some timestamping macros with actual information
CFLAGS+=-Wno-builtin-macro-redefined \
	-D__BUILDTIME__=$(shell date -u +'"\"%Y-%m-%dT%H:%M:%SZ\""') \
	-D__BUILDUSER__=$(shell printf '"\\"%s\\""' `id -nu`)

# Where the org-tangle binary resides?  In this case we are using the
# development version.
#ORG_TANGLE=$(HOME)/st/src/org-tangle/src/org-tangle
#ORG_TANGLE=$(HOME)/st/repo/sc/src/org-tangle/src/org-tangle
ORG_TANGLE=org-tangle
ORG_WEAVE=org-weave

SOURCES	= comp.c cpu_6502.c sc65.c #cpu_6502.c acia6850.c term.c
OBJECTS	= $(SOURCES:.c=.o)
EXECUTABLES = comp test_cpu_6502 sc65
orgs	= $(wildcard *.org)
pdfs	= $(orgs:.org=.pdf)
htmls	= $(orgs:.org=.html)
txts	= $(orgs:.org=.txt)
outputs = $(addprefix $(output)/pdf/,$(pdfs)) $(addprefix $(output)/txt/,$(txts))

# Directory for created pdf, html and texts.
output=$(HOME)/st/doc
VPATH = $(output)

# Other pandoc options to be considered:
#
PANDOC_OPTS= --tab-stop=8 --top-level-division=chapter
PANDOC_PDF_OPTS	= $(PANDOC_OPTS) -V geometry:a4paper,margin=3cm
PANDOC_PDF_OPTS += --number-sections --toc --toc-depth=3
ifeq ($(SYSTEM), Darwin)
PANDOC_PDF_OPTS += --pdf-engine xelatex
else
PANDOC_PDF_OPTS += --highlight-style tango
endif

PANDOC_PDF_OPTS +=  $(COLOR_LINKS)
PANDOC_TXT_OPTS  = $(PANDOC_OPTS) -t plain
PANDOC_HTML_OPTS = $(PANDOC_OPTS) -t html -N
COLOR_LINKS = -V colorlinks=true -V linkcolor=blue -V urlcolor=red -V toccolor=gray

default: all

all:	$(SOURCES) $(EXECUTABLES) $(outputs)

clean:
	rm -v $(OBJECTS) $(EXECUTALBES) Makefile.deps *~ $(pdfs) $(txts)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

comp: comp.o cpu_6502.o term.o
	$(CC) $(LDFLAGS) $^ -o $@

test_cpu_6502: test_cpu_6502.o cpu_6502.o
	$(CC) $(LDFLAGS) $^ -o $@

# Binary of Minimal SC/65 computer for testing firmware.
sc65: sc65.o cpu_6502.o term.o
	$(CC) $(LDFLAGS) $^ -o $@

# Tangle C source files
comp.h comp.c cpu.h cpu_6502.h cpu_6502.c : scsim.org
	$(ORG_TANGLE) scsim.org
	./build-asm-tests.sh		# Build test files

-include Makefile.deps
Makefile.deps:
	$(CC) $(CFLAGS) -MM *.[c] > Makefile.deps

### Rule to weave the .org file to .worg file
%.worg: %.org
	$(ORG_WEAVE) -f -o $@ $<

### Pandoc Rules for worg files
# NOTICE that I supplied '--from=org' option as pandoc do not
# necesarily recognize '.worg' files.
#
$(output)/txt/%.txt: %.worg
	pandoc $(PANDOC_TXT_OPTS) --from=org -o $@ $<
	cp $@ .

$(output)/pdf/%.pdf: %.worg
	pandoc $(PANDOC_PDF_OPTS) --from=org -o $@ $<
	cp $@ .

# ORG to TXT (UTF-8) transformation
# %.txt: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-ascii-export-to-ascii

# # ORG to PDF usign LaTeX export
# %.pdf: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-latex-export-to-pdf
# 	rm -v $(basename $@).tex

# # ORG to HTML
# %.html: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-html-export-to-html

#Eof:$Id: Makefile,v 1.8 2023-10-17 18:23:16+02 radek Exp $
