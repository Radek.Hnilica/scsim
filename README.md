# SCSIM

## The scsim project.
Simulator of  old processor(s),  and computer(s) which  never existed.
Optionally also processors which never existed.

For more please see
[scsim.org](https://gitlab.com/Radek.Hnilica/scsim/-/blob/main/scsim.org)
file instead.  Be aware that GitLab is not able to render it correctly
as I use complicated construction refering to files which are
generated and as such are not commited into git.  To see better you
can open
[scsim.pdf](https://gitlab.com/Radek.Hnilica/scsim/-/blob/main/scsim.pdf).

## Why?
This is mostly experiment in _literal programming_.  It is overwritten
few times and there will be overhauls  in future as I hit the wall and
I'm forced to do it.

## Status / Results
At this  point I'm able  to start  the `sc65test` program  which loads
some firmware  into simulated  computer based  on 6502.   The firmware
contains Machine Code Monitor so I can play with the virtual computer.

## Building
This is another experimental project which has not much an use yet.
It is written in style of *literal programming*, like it or not, and
as such needs tangle tools to extract the source code from it.  The
tangle tools I use are
[org-tangle](https://gitlab.com/radek_hnilica/org-tangle) and
[md-tangle](https://gitlab.com/radek_hnilica/md-tangle).  You can find
them in my [GitLab](https://gitlab.com/radek_hnilica).

Be aware  that the tools,  as this  project itself are  not production
quality.

To transform the files I also use Makefile and scripts.

Because of complexity and dependency, simple run of `make` does not do
all the  things.  For  that a  script `build` have  to be  used.  This
script do  not exist yet.  I  plan to restructure some  of my projects
and make unified calls to build scripts for them.
