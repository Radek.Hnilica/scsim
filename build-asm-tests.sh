#!/bin/bash
# $Id: build-asm-tests.sh,v 1.3 2023/09/27 17:03:29 radekhnilica Exp $
# $Source: /Users/radekhnilica/st/repo/sc/src/scsim/src/build-asm-tests.sh,v $
# Assembly test code examples
#
# Copyright (c) 2022 Radek Hnilica
# All rights reserved.
# License: GPLv3, or ask me.

ASMDIR=asm
ASMOPTS="-C 6502"

#set -x
for f in $ASMDIR/*.a65; do
	asmx $ASMOPTS -l ${f/%.a65/.lst} -o ${f/%.a65/.hex} $f 2>&1 |grep -v ^Pass
	expand ${f/%.a65/.lst} >${f/%.a65/.prn}
	#asmx $ASMOPTS $f -l ${f/%.a65/.lst} -s9 -o ${f/%.a65/.s9} 2>/dev/null
done
