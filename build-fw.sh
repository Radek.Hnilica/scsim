#!/bin/bash
# $Id: build-fw.sh,v 0.0 2022/08/30 11:12:36 radek Exp radek $
# $Source: /home/radek/st/src/small/scsim/src/build-fw.sh,v $
# Build firmware
#
# Copyright (c) 2022 Radek Hnilica
# All rights reserved.
# License: GPLv3, or ask me.

ASMDIR=sysfw
ASMOPTS="-C 6502"

#set -x
for f in $ASMDIR/*.a65; do
	asmx $ASMOPTS $f -l ${f/%.a65/.lst} -o ${f/%.a65/.hex} 2>&1 | grep -v ^Pass
	hex2bin ${f/%.a65/.hex} >/dev/null 2>&1
	expand ${f/%.a65/.lst} | egrep "^[0-9A-F]{4} " >${f/%.a65/.prn}
	#asmx $ASMOPTS $f -l ${f/%.a65/.lst} -s9 -o ${f/%.a65/.s9} 2>/dev/null
done
